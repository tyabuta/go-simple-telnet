binName    = simple-telnet
cmdPackage = ./cmd/simple-telnet
repo       = gitlab.com/tyabuta/go-simple-telnet

VERSION    = 0.1.0
REV        = $(shell git rev-parse --verify HEAD)
BUILT_AT   = $(shell date '+%Y-%m-%d %H:%M:%S %Z')
GO_VERSION = $(shell go version)

X_VERSION    = -X $(repo).version=$(VERSION)
X_REV        = -X $(repo).revision=$(REV)
X_BUILT_AT   = -X \"$(repo).builtAt=$(BUILT_AT)\"
X_GO_VERSION = -X \"$(repo).goVersion=$(GO_VERSION)\"

LDFLAGS = -ldflags "-w -s $(X_VERSION) $(X_REV) $(X_BUILT_AT) $(X_GO_VERSION)"


run-client:
	go run $(cmdPackage)/main.go localhost 5555

run-server:
	go run $(cmdPackage)/main.go -s -P 5555

run-help:
	go run $(cmdPackage)/main.go --help


go-dep:
	go get -v -u github.com/urfave/cli
	go get -v -u github.com/reiver/go-telnet


build:
	go build $(LDFLAGS) -o bin/$(binName) $(cmdPackage)

build-mac:
	GOOS=darwin  GOARCH=amd64 go build $(LDFLAGS) -o bin/darwin64/$(binName)      $(cmdPackage)
build-linux64:
	GOOS=linux   GOARCH=amd64 go build $(LDFLAGS) -o bin/linux64/$(binName)       $(cmdPackage)
build-win64:
	GOOS=windows GOARCH=amd64 go build $(LDFLAGS) -o bin/windows64/$(binName).exe $(cmdPackage)
build-win32:
	GOOS=windows GOARCH=386   go build $(LDFLAGS) -o bin/windows32/$(binName).exe $(cmdPackage)

build-all:
	@make build-mac
	@make build-linux64
	@make build-win64
	@make build-win32

clean:
	rm -rf bin

.PHONY: build clean

