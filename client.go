package simpletelnet

import (
	"github.com/reiver/go-telnet"
	"github.com/reiver/go-oi"
	"bufio"
	"os"
	"fmt"
)

func client() error {
	fmt.Printf("Dial to %s:%d\n", Option.Host, Option.Port)
	return telnet.DialToAndCall(fmt.Sprintf("%s:%d", Option.Host, Option.Port), caller{})
}

type caller struct{}

func (c caller) CallTELNET(ctx telnet.Context, w telnet.Writer, r telnet.Reader) {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		oi.LongWrite(w, scanner.Bytes())
		oi.LongWrite(w, []byte("\n"))
	}
}
