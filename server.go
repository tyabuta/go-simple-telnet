package simpletelnet

import (
	"github.com/reiver/go-telnet"
	"fmt"
)

func server() error {
	fmt.Printf("Telnet server start on port:%d\n", Option.Port)
	return telnet.ListenAndServe(fmt.Sprintf(":%d", Option.Port), handler{})
}

type handler struct{}

func (h handler) ServeTELNET(ctx telnet.Context, w telnet.Writer, r telnet.Reader) {
	println("-- connect --")

	var buffer [1]byte
	p := buffer[:]

	for {
		n, err := r.Read(p)
		if n > 0 {
			bytes := p[:n]
			print(string(bytes))
		}

		if nil != err {
			break
		}
	}

	println("-- disconnect --")
}
