package simpletelnet

import (
	"fmt"
	"github.com/urfave/cli"
	"log"
)

var App *cli.App

var (
	version   = "xxxxxxxxx"
	revision  = "xxxxxxxxx"
	builtAt   = "xxxxxxxxx"
	goVersion = "xxxxxxxxx"
)

type option struct {
	Host       string
	Port       int
	ServerMode bool
}

var Option = option{}

var HelpTemplate = `NAME:
   {{.Name}}{{if .Usage}} - {{.Usage}}{{end}}

USAGE:
   {{if .UsageText}}{{.UsageText}}{{else}}{{.HelpName}} {{if .VisibleFlags}}[options]{{end}}{{end}}{{if .Version}}{{if not .HideVersion}}

VERSION:
   {{.Version}}{{end}}{{end}}{{if .Description}}

DESCRIPTION:
   {{.Description}}{{end}}{{if len .Authors}}

AUTHOR{{with $length := len .Authors}}{{if ne 1 $length}}S{{end}}{{end}}:
   {{range $index, $author := .Authors}}{{if $index}}
   {{end}}{{$author}}{{end}}{{end}}{{if .VisibleCommands}}

OPTIONS:
   {{range $index, $option := .VisibleFlags}}{{if $index}}
   {{end}}{{$option}}{{end}}{{end}}

`

func init() {
	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Printf("version %s (rev:%s)\n", c.App.Version, revision)
		fmt.Printf("built by %s at %s\n", goVersion, builtAt)
	}

	App = cli.NewApp()
	App.CustomAppHelpTemplate = HelpTemplate
	App.Name = "simple-telnet"
	App.Usage = "Simple telnet server and client."
	App.Version = version
	App.Author = "tyabuta"
	App.Email = "gmforip@gmail.com"
	App.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "host, H",
			Value:       "localhost",
			Destination: &Option.Host,
			Usage:       "Connect to Host."},
		cli.IntFlag{
			Name:        "port, P",
			Value:       5555,
			Destination: &Option.Port,
			Usage:       "Connect or Listen to Port."},
		cli.BoolFlag{
			Name:        "server, s",
			Destination: &Option.ServerMode,
			Usage:       "Server Mode."},
	}
	App.Action = execute
}

func execute(_ *cli.Context) error {
	if Option.ServerMode {
		if err := server(); nil != err {
			log.Fatal(err)
			return nil
		}
	}

	if err := client(); nil != err {
		log.Fatal(err)
		return nil
	}

	return nil
}
